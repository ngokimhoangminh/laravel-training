* Abstract :
	+ Lớp Abstract  được xem như một class cha cho tất cả các Class có cùng bản chất.
	+ Mỗi lớp con chỉ có thể kế thừa từ một lớp trừu tượng bằng từ khóa extends.
	+ Các phương thức ( hàm ) khi được khai báo là abstract thì chỉ được định nghĩa chứ không được phép viết code xử lý trong phương thức.
		Nội dung sẻ được viết trong class kế thừa khi override lại phương thức đó.
	+ Nhưng nếu không phải là phương thức abstract thì vẫn khai báo và viết code được như bình thường.
	+ Phương thức abstract chỉ có thể khai báo trong abstract class.
	+ Không thể khởi tạo một abstract class.
	+ Các lớp kế thừa một abstract class phải định nghĩa lại tất cả các phương thức trong abstract class đó.
	+ Abstract class không hỗ trợ đa kế thừa.
* Interface:
	+ Interface không phải là 1 lớp. 
	+ Nó được mô tả như là 1 bản thiết kế cho các class có chung cách thức hoạt động.
	+ Trong interface chúng ta có thể khai báo được hằng nhưng không thể khai báo biến.
	+ Trong interface chúng ta chỉ được khai báo phương thức chứ không được định nghĩa chúng. Các phương thức chỉ khai báo tên hàm và không viết nội dung hàm trong đó.
	+ Một interface không thể khởi tạo được (vì nó không phải là một đối tượng).
	+ Một lớp có thể kế thừa từ nhiều interface khác nhau bằng từ khóa implements.
	+ Lớp con kế thừa từ interface sẻ phải override tất cả các phương thức trong đó.
	+ Các interface có thể kế thừa lẫn nhau (dùng extend).
	+ Interface có hỗ trợ đa kế thừa.
* Index SQL:
	- Chỉ mục (INDEX) trong SQL là bảng tra cứu đặc biệt mà công cụ tìm kiếm cơ sở dữ liệu
 		có thể sử dụng để tăng nhanh thời gian và hiệu suất truy xuất dữ liệu.
	- Cú pháp cơ bản của lệnh CREATE INDEX trong SQL như sau: 
		CREATE INDEX index_name
		ON table_name (column1, column2, ...);

	Ví dụ bạn đang có table users với các cột:
		id (Primary Key, Auto Increament)
		username
		email
		password

	- Single-Column Index được tạo cho duy nhất 1 cột trong bảng. Cú pháp cơ bản như sau:
		CREATE INDEX ten_index
		ON ten_bang (ten_cot);
	
		vd: CREATE INDEX idx_username
	     	ON users (username);

	-Unique Index là chỉ mục duy nhất, được sử dụng để tăng hiệu suất tìm kiếm dữ liệu. 
	Một chỉ mục duy nhất không cho phép chèn bất kỳ giá trị trùng lặp nào được chèn vào bảng. Cú pháp cơ bản như sau.
		CREATE UNIQUE INDEX ten_index
		ON ten_bang (ten_cot);

		vd: CREATE UNIQUE INDEX idx_id
	     	ON users (id);

	-Composite Index là chỉ mục kết hợp dành cho hai hoặc nhiều cột trong một bảng. Cú pháp cơ bản của nó như sau:
		CREATE INDEX ten_index
		ON ten_bang (cot1, cot2);

		vd: CREATE INDEX idx_username_email
	     	ON users (username, email);

	-Implicit Index (Index ngầm định) là chỉ mục mà được tạo tự động bởi Database Server khi một bảng được tạo.
	-Khi không cần sử dụng INDEX nữa bạn có thể DROP theo cú pháp sau:

		DROP INDEX ten_index;
