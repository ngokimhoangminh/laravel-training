<?php

namespace Database\Factories;

use App\Models\Categories;
use Illuminate\Database\Eloquent\Factories\Factory;

use Faker\Generator as Faker;

class CategoriesFactory extends Factory
{
	/**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Categories::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
	        'image' => $this->faker->imageUrl($width = 200,$height = 300,'',true,'Faker'),
	        'description' => $this->faker->text,
	        'created_at' => now(),
	        'updated_at' => now()
	        ];
    }
}
