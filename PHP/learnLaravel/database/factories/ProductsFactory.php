<?php

namespace Database\Factories;

use App\Models\Products;
use Illuminate\Database\Eloquent\Factories\Factory;

use Faker\Generator as Faker;

class ProductsFactory extends Factory
{
	/**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Products::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
	        'price' => $this->faker->numberBetween(100000,3000000),
	        'description' => $this->faker->text,
	        'image' => $this->faker->imageUrl($width = 200,$height = 300,'',true,'Faker'),
	        'created_at' => now(),
	        'updated_at' => now()
        ];
    }
}

