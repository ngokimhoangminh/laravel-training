<?php

namespace Database\Factories;

use App\Models\Colors;
use Illuminate\Database\Eloquent\Factories\Factory;

class ColorsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Colors::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'color' => $this->faker->name,
            'hex' => $this->faker->numberBetween(100000,999999),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
