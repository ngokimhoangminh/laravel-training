<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Models\ProductColor::factory()->count(30)->create(); 
    }
}
