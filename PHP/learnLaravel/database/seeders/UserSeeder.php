<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //\App\Models\User::factory()->count(30)->create();
        User::create([
            'name' =>'HoangMinh',
            'email' => 'hoangminhcp10@gmail.com',
            'password' => bcrypt('123456'),
            'role_id' => '1',
            'remember_token' => 'administrator'
        ]);

        User::create([
            'name' =>'HoangNam',
            'email' => 'hoangnam@gmail.com',
            'password' => bcrypt('123456'),
            'role_id' => '2',
            'remember_token' => 'administrator'
        ]);
        User::create([
            'name' =>'DiemQuynh',
            'email' => 'DiemQuynh@gmail.com',
            'password' => bcrypt('123456'),
            'role_id' => '3',
            'remember_token' => 'administrator'
        ]);
    }
}
