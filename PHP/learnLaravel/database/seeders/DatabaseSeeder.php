<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(ProductsSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(ColorsSeeder::class);
        $this->call(ProductColorSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TaskSeeder::class);
    }
}
