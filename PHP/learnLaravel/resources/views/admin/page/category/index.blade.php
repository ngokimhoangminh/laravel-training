@extends('admin.layouts.main', ['title' => 'Danh mục'])
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.page.view-header',['title_view'=>'Danh mục'])

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header d-flex align-items-center ">
                <h3 class="card-title col-md-10">Danh mục sản phẩm</h3>
                <div class="col-md-2 d-flex justify-content-center">
                    <button class="btn btn-info">Thêm</button>
                </div>
              </div>
              <!-- /.card-header -->
             
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@push('script')
<script type="text/javascript">

</script>
@endpush