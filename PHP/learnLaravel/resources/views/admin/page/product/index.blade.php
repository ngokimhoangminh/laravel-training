@extends('admin.layouts.main', ['title' => 'Sản Phẩm'])
@push('styles')
<style type="text/css">
    .btnDetail{
      width: 67px;
      height: 34px;
      border-radius: 25px;
    }
</style>
@endpush
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.page.view-header',['title_view'=>'Sản Phẩm'])

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header d-flex align-items-center ">
                <h3 class="card-title col-md-10">Danh sách thông tin sản phẩm</h3>
                <div class="col-md-2 d-flex justify-content-center">
                    <button class="btn btn-info">Thêm</button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="datatable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th></th>
                      <th>STT</th>
                      <th>Sản Phẩm</th>
                      <th>Giá</th>
                      <th>Mô Tả</th>
                      <th>Hình Ảnh</th>
                      <th style="width: 130px;" class="text-center">Thao Tác</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach ($products as $key => $product)
                    <tr>
                      <td class="align-middle">
                        @if(count($product->colors)>0)
                          <button class="btn btn-success btnDetail" data-id="{{$key+1}}">
                            <i class="fas fa-list text-white"></i>&nbsp;{{count($product->colors)}}
                          </button>
                        @endif
                      </td>
                      <td class="align-middle">{{$key+1}}</td>
                      <td class="align-middle">{{$product->name}}</td>
                      <td class="align-middle">{{number_format($product->price,0,',','.')}}&nbsp;đ</td>
                      <td class="align-middle">{{$product->description}}</td>
                      <td class="text-center"><img src="{{$product->image}}" class="w-75"></td>
                      <td class="pt-3 text-center align-middle">
                        <a href="{{route('admin.product.edit',$product)}}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                        <a href="#" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                      </td>
                    </tr>
                    <tr class="childRow" style="display: none;"> 
                      <td colspan="7">
                        <table class="tbItems" rules="rows"> 
                          <thead> 
                            <tr> 
                              <th>ID</th> 
                              <th>Name</th> 
                              <th>Hex</th>
                              <th>Color</th>  
                            </tr> 
                          </thead> 
                          <tbody> 
                            @foreach ($product->colors as $color)
                               <tr> 
                                  <td>{{$color->id}}</td> 
                                  <td>{{$color->color}}</td> 
                                  <td>{{$color->hex}}</td> 
                                  <td style="background:#{{$color->hex}}"></td> 
                              </tr> 
                            @endforeach 
                          </tbody> 
                        </table> 
                      </td> 
                
                  </tr>
                  @endforeach
                  </tbody>
                </table>

                 
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="row">
                    {!! $products->links() !!}
                </div>
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@push('script')
<script type="text/javascript">
// $('#datatable').DataTable();
$(".btnDetail").click(function(){ 
  var detail = $(this).parent().parent().next();
  var status = $(detail).css("display"); 
  if(status == "none") 
    $(detail).css("display", "block"); 
  else 
    $(detail).css("display", "none");
});
</script>
@endpush