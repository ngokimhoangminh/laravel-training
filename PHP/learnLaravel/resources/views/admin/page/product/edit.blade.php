@extends('admin.layouts.main', ['title' => 'Sản Phẩm'])
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.page.view-header',['title_view'=>'Sữa Sản Phẩm'])
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
          <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header d-flex align-items-center ">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
              </div>
              <div class="card-body">
                <form method="POST" action="{{route('admin.product.update',[$product->id])}}"  enctype='multipart/form-data'>
                      @method('PUT')
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Sản Phẩm</label>
                        <input type="text" class="form-control" value="{{$product->name}}" name="name" id="slug" aria-describedby="emailHelp">
                        
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Giá</label>
                        <input type="text" class="form-control" value="{{number_format($product->price,0,',','.')}}" name="name" id="slug" aria-describedby="emailHelp">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mô tả sản phẩm</label>
                        <input type="text" class="form-control" value="{{$product->description}}" name="description" id="exampleInputEmail1" aria-describedby="emailHelp">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Hình Ảnh</label>
                        <img src="{{$product->image}}" alt="" class="w-15">
                      </div>
                      <button type="submit" class="btn btn-info">Cập Nhật</button>
                  </form> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
@push('script')
<script type="text/javascript">
</script>
@endpush