@extends('admin.layouts.main', ['title' => 'Nhiệm Vụ'])
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.page.view-header',['title_view'=>'Nhiệm Vụ'])
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header d-flex align-items-center ">
                <h3 class="card-title col-md-3">Danh sách nhiệm vụ được giao</h3>
                <div class="col-md-7 d-flex">
                 {{--  <form action="{{route('admin.task.list')}}" method="GET" class="d-flex w-75" id="myForm">
                  {{csrf_field()}}
                     <div class="input-group-prepend">
                        <button class="btn btn-info"><i class="fas fa-search"></i></button>
                    </div>
                    <input type="text" class="form-control" placeholder="Nhập tìm kiếm..." aria-label="Username" aria-describedby="basic-addon1" name="search" id="search" >
                  <button class="btn btn-info ml-2" onclick=""><i class="fas fa-spinner"></i></button>
                </form> --}}
                </div>
                <div class="col-md-2 d-flex justify-content-center">
                  @can('create',\App\Models\Task::class)
                    <a class="btn btn-info" href="{{route('admin.task.create')}}">Thêm</a>
                  @endcan 
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="datatable" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>STT</th>
                    <th>Nhiệm Vụ</th>
                    <th>Mô Tả</th>
                    <th style="width: 115px;" class="text-center">Thao Tác</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($task as $key => $value)
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->description}}</td>
                    <td class="pt-3 text-center d-flex justify-content-center">
                      @can('update',$value)
                        <a href="{{route('admin.task.edit',[$value->id])}}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                      @endcan
                      @can('delete',$value)
                      <form action="{{route('admin.task.destroy',[$value->id])}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <a onclick="return confirm('Bạn muốn xóa nhiệm vụ này không?');" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                      @endcan
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>

              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@push('script')
<script type="text/javascript">
  $('#datatable').DataTable();
  function reset()
  {
    console.log("minh");
    document.getElementById("myForm").reset();
  }
</script>
@endpush