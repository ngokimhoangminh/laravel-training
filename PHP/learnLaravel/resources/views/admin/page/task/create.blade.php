@extends('admin.layouts.main', ['title' => 'Nhiệm Vụ'])
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('admin.page.view-header',['title_view'=>'Thêm Nhiệm Vụ'])
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header d-flex align-items-center ">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
              </div>
              <div class="card-body">
                <form method="POST" action="{{route('admin.task.store')}}"  enctype='multipart/form-data'>
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tên nhiệm vụ</label>
                        <input type="text" class="form-control" value="{{old('name')}}" name="name" id="slug" aria-describedby="emailHelp" placeholder="Tên danh mục...">
                        
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mô tả danh mục</label>
                        <input type="text" class="form-control" value="{{old('description')}}" name="description" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Mô tả nhiệm vụ">
                      </div>
                      <button type="submit" class="btn btn-primary">Thêm</button>
                    </form> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
@push('script')
<script type="text/javascript">
</script>
@endpush