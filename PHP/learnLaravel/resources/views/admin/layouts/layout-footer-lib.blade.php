<!-- jQuery -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src="{{asset('libs/jquery/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('libs/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('libs/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> --}}
<script src="{{asset('libs/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('libs/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('libs/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('libs/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('libs/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('libs/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('libs/moment/moment.min.js')}}"></script>
<script src="{{asset('libs/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('libs/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('libs/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('libs/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('libs/adminlte/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('libs/adminlte/dashboard.js')}}"></script>
<!-- Toast -->
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<!--Datatable-->
<script src="{{asset('libs/jquery/jquery.min.js')}}"></script>
<script src="{{asset('libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('libs/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('libs/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('libs/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
  @if(session()->has('success'))
    toastr.success('Đăng nhập thành công.', 'Thành công!');
  @endif
  
  $.extend(true, $.fn.dataTable.defaults,{
		"aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "Tất Cả"]],
    	"iDisplayLength": 10,
    	"language": {
	        "sLengthMenu": "Hiển thị _MENU_ record trên 1 trang",
	        "sZeroRecords": "Không tìm thấy dữ liệu",
	        "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
	        "sInfoEmpty": "Không có dữ liệu nào",
	        "sInfoFiltered": "(được lọc từ tổng sô _MAX_ trong dữ liệu)",
	        "sSearch": "Tìm kiếm:",
	        "sShow": "Hiển Thị:",
	        "oPaginate": {
            "sNext": "Sau",
            "sPrevious": "Trước"
        },
    },
  });
</script>
@stack('script')