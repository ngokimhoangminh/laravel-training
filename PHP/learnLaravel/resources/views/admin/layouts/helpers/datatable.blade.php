@section('css_common')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<link rel="stylesheet" href="{{ asset('libs/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('libs/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@append
@section('js_common')
<script src="{{asset('libs/jquery/jquery.min.js')}}"></script>
<script src="{{asset('libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('libs/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('libs/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('libs/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
@append

