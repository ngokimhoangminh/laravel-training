<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\HomeController;



Route::middleware(['auth'])->group(function (){
	Route::get('/', [HomeController::class, 'index']);
	Route::group([
		'as'=>'admin.',
		'middleware'=>'is_admin',
		'prefix'=>'admin'
	],function(){

		Route::get('/', [\App\Http\Controllers\HomeController::class,'index']);

		Route::resource('/product', \App\Http\Controllers\ProductController::class);

		Route::resource('/category', \App\Http\Controllers\CategoryController::class);

		Route::resource('/task', \App\Http\Controllers\TaskController::class);

		// Route::get('/category', [\App\Http\Controllers\CategoryController::class,'index'])->name('category.list')->middleware(['can:admin']);

		Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class,'logout'])->name('logout');

	});
	
});



Route::get('/admin/login', function () {
    return view('admin.page.auth.login');
})->name('login');

Route::post('/admin/login', [\App\Http\Controllers\Auth\LoginController::class,'login'])->name('login');


