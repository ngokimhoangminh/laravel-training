<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class Task extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'name','description'];
    protected $primaryKey = 'id';
 	protected $table = 'tasks';

 	public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
