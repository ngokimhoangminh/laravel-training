<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Products;
use App\Models\ProductColor;

class Colors extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
 	protected $table = 'colors';
 	public function product()
 	{
 		return $this->belongsToMany(Products::class,ProductColor::class,'color_id','product_id');
 	}
}
