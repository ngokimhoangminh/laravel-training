<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Colors;
use App\Models\ProductColor;

class Products extends Model
{
    use HasFactory;
    //có thể insert vào
    protected $fillable = ['name', 'price','description','image'];
    protected $primaryKey = 'id';
 	protected $table = 'products';

 	public function colors()
 	{
 		return $this->belongsToMany(Colors::class,ProductColor::class,'product_id','color_id');
 	}
}
