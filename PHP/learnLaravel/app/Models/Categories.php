<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;
    //có thể insert vào
    protected $fillable = ['name', 'image','description'];
    protected $primaryKey = 'id';
 	protected $table = 'categories';
}
