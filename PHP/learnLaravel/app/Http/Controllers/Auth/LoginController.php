<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Session;
session_start();
class LoginController extends Controller
{
    //
    public function login(Request $request)
    {
    	$credentials=$request->validate([
    		'email'=>['required','email'],
    		'password'=>['required']
    	],[
    		'email.required' => 'Email không được để trống',
    		'email.email'=> 'Email không đúng định dạng',
    		'password.required' => 'Password không đúng định dạng'
    	]);

    	if(Auth::attempt($credentials))
    	{
    		//$request->session()->regenerate();
            session()->flash('success');
            $user=Auth::user();
    		return redirect()->intended('admin');//Lưu trữ chuyển hướng trước đó, nếu k có thì vào /admin
    	}
    	return back()->withErrors([
    		'message'=>'Thông tin đăng nhập không đúng, vui lòng kiểm tra lại.'
    	])->withInput();
    }

    public function logout()
    {
    	Auth::logout();
		session()->flush();
    	return redirect()->route('login');
    }
}
