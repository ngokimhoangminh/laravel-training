<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\Task;
use App\Models\Role;
use App\Policies\TaskPolicy;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        //Task::class => TaskPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('category', fn($user) => $user->role_id==Role::IS_ADMIN || $user->role_id==Role::IS_MANAGER);
        // Gate::define('admin', function($user){
        //     //return $user->role==='admin';
        //     if ($user->role == "admin"){
        //         return true;
        //     }
        //     return false;
        // });
        // Gate::define('manage', function($user){
        //     if ($user->role == "admin" || $user->role == "manager") {
        //         return true;
        //     }
        //     return false;
        // });
    }
}
