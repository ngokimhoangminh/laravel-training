1. Scope
	- Khái niệm: Scope là một phương thức hỗ trợ ta thêm 1 điều kiện nào đó để quá trình query diễn ra gọn gàng nhanh chóng hơn. 
		Để ta đỡ phải gọi method where gây dài dòng.
	- Cách khai báo: ta sẽ tạo Scope thông qua 1 method public. Có các loại Scope là: Global scope và Anonymous globel scope, Local scope.
		vd về Local Scope: Local scope bắt buộc phải return về một query builder và đặt tên với mẫu như sau: scope{Name}
				   public function scopeFindEmail($query, $email)
    		       		  {
       		       		       return $query->where("Email", $email); 
   		     		  }
	- Cách sử dụng Scope: Lúc này bạn muốn sử dụng scope nào bạn chỉ cần gọi tên scope đó, bỏ chữ "scope"
			$email = UserModel::findEmail($email)->get();
2. Eager Loading
	+ Eager loading là một feature của Eloquent, nó sẽ query các relations ngay khi query của parent model được thực thi xong. 
		Điều này sẽ giúp hạn chế vấn đề N+1 query của relationship trong Eloquent.
	+ Sử dụng eager loading trong Eloquent các bạn sử dụng phương thức with(), ngoài ra còn có thể sử dụng phương thức load().
		vd: $products=Products::with('colors')->get();
		     $products=Products::all()->load('colors'); // sẽ chạy phương thức load ra các bản ghi đầu tiên.