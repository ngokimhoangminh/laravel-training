-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 01, 2021 lúc 08:29 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `laravel`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Prof. Betty Treutel', 'https://via.placeholder.com/200x300.png/0088ee?text=+Faker+dolores', 'Mollitia nihil explicabo nulla ducimus modi voluptate. Ut incidunt totam dolores. Maiores facilis aspernatur ut iusto sunt perferendis et. Voluptatibus ullam quidem porro sapiente deserunt harum.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(2, 'Kristy Weber', 'https://via.placeholder.com/200x300.png/0022ee?text=+Faker+et', 'Aspernatur omnis praesentium quia velit dolorem et. Rerum qui quo cum dicta. Molestias ratione sint enim non sint necessitatibus. Voluptatibus ipsam laborum quisquam.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(3, 'Dr. Cruz Green', 'https://via.placeholder.com/200x300.png/00ff66?text=+Faker+rerum', 'Quas nemo earum expedita porro. Sed necessitatibus est et sit enim quidem. Sint impedit qui autem quia voluptatum. Qui dignissimos perspiciatis praesentium consectetur eius quibusdam.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(4, 'Dr. Russell O\'Kon Jr.', 'https://via.placeholder.com/200x300.png/0022dd?text=+Faker+soluta', 'In tempora totam sunt distinctio omnis cum aspernatur. Magni eos et id quasi quidem aspernatur minima sit. Incidunt aut rem accusantium velit. Adipisci minus deserunt ut possimus sequi qui omnis sed.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(5, 'Gene Heidenreich', 'https://via.placeholder.com/200x300.png/0099ee?text=+Faker+quia', 'Dolorum vel at in exercitationem inventore. Voluptatem impedit autem eum itaque et quia soluta. Ex repellat et perferendis minima voluptas.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(6, 'Reggie Larson', 'https://via.placeholder.com/200x300.png/00aa33?text=+Faker+rerum', 'Sapiente dolorem dolore ad fuga totam beatae. Et quod excepturi aut. Culpa qui aliquam dolore labore velit. Dolor provident sit dicta molestias similique non sapiente.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(7, 'Tre Wolf', 'https://via.placeholder.com/200x300.png/002266?text=+Faker+autem', 'Molestias delectus eveniet iure ut vel doloribus officiis. Excepturi veniam non esse est omnis omnis. Porro iste tempora cum perferendis qui quia. Molestias iste et aperiam.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(8, 'Emmie Swaniawski', 'https://via.placeholder.com/200x300.png/006600?text=+Faker+vel', 'Maxime voluptatibus qui quas accusantium. Ipsa et veritatis et. Quos itaque est qui non itaque eligendi harum. Accusamus mollitia voluptates velit quia.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(9, 'Dianna Kovacek', 'https://via.placeholder.com/200x300.png/00ee33?text=+Faker+expedita', 'Aut qui officia ut quia quia. Ratione est rerum et magnam velit explicabo. Ipsam eaque molestiae error consequatur nesciunt. Ut corporis ut suscipit.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(10, 'Ms. Glenna Feest III', 'https://via.placeholder.com/200x300.png/006633?text=+Faker+ea', 'Minima totam ipsam veritatis nobis et provident. Repellendus reprehenderit explicabo ut. Ipsam ut modi ea. Velit eos perspiciatis enim voluptatem voluptate illum soluta.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(11, 'Breana Murazik', 'https://via.placeholder.com/200x300.png/001199?text=+Faker+nam', 'Quisquam consequatur fugiat provident molestias exercitationem veniam. Neque animi qui beatae ea doloribus porro. Sit repudiandae ipsa vel repudiandae.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(12, 'Meda Padberg', 'https://via.placeholder.com/200x300.png/00aabb?text=+Faker+alias', 'Magnam aliquid dolores ullam dolores voluptate magnam et. Sed repellat optio aut fugit beatae saepe esse.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(13, 'Malachi Weimann', 'https://via.placeholder.com/200x300.png/005522?text=+Faker+enim', 'Autem est autem dolorem occaecati voluptatum sed aut. Voluptatem ab dicta sint et quia. Doloremque atque doloribus tempora. Aspernatur eum mollitia et eligendi.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(14, 'Maye Tillman', 'https://via.placeholder.com/200x300.png/00aa99?text=+Faker+laboriosam', 'Nulla fugit enim fuga unde sunt. Ad non in fugiat cupiditate. Itaque magni odit officia fugit nihil delectus iusto. Ipsum quibusdam totam odit voluptas est. Ut ut et saepe tempore.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(15, 'Prof. Ola Terry PhD', 'https://via.placeholder.com/200x300.png/0033dd?text=+Faker+hic', 'Reiciendis vel eveniet fuga aut. Tempore aliquid non placeat iste id. Debitis eum et iure.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(16, 'Horacio Bahringer', 'https://via.placeholder.com/200x300.png/0066ee?text=+Faker+officia', 'Cum commodi quo cum. Error voluptas debitis quis ratione ea quod. Sapiente modi iusto repudiandae sequi non culpa. Tenetur earum velit corrupti expedita perferendis et error.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(17, 'Mr. Elmore Turner', 'https://via.placeholder.com/200x300.png/008844?text=+Faker+occaecati', 'Nemo voluptates ratione et et distinctio impedit amet. Saepe eos neque alias rerum voluptas vel hic. Error alias eligendi id ut quia beatae quis.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(18, 'Kris Simonis', 'https://via.placeholder.com/200x300.png/0066aa?text=+Faker+optio', 'Esse accusantium aut necessitatibus et est nemo placeat et. Est laboriosam nesciunt vel temporibus enim dolorum rerum.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(19, 'Ray Mayer', 'https://via.placeholder.com/200x300.png/0099cc?text=+Faker+impedit', 'Esse quia illo corrupti accusamus earum. Aperiam tenetur aliquam quibusdam corporis sunt expedita quis quis. Voluptatum dicta id et ducimus minima.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(20, 'Elisha Nienow', 'https://via.placeholder.com/200x300.png/0088aa?text=+Faker+atque', 'Quo eos architecto quo neque debitis neque. Iusto delectus enim eligendi earum ipsam molestiae cum. Ratione deserunt ipsa qui cupiditate. Ab non molestiae ea. Sint inventore nam aut commodi enim.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(21, 'Prof. Yasmin Pfannerstill', 'https://via.placeholder.com/200x300.png/00ee66?text=+Faker+ut', 'Sit rem et qui aliquam. Dignissimos blanditiis placeat eum. Velit iure exercitationem vero ex minima. Labore in ut maiores dolore.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(22, 'Madisen Littel V', 'https://via.placeholder.com/200x300.png/0011bb?text=+Faker+alias', 'Et blanditiis sint nesciunt dolores illum quia sint. Recusandae ut aut ut maiores. Rerum dolorum et omnis aut dolorum eos maxime minima. Velit quisquam dolore magni non voluptatem.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(23, 'Prof. Rudolph Collier', 'https://via.placeholder.com/200x300.png/00ee66?text=+Faker+modi', 'Voluptas quia earum quia omnis illo maxime. Ut accusantium nesciunt dolorem exercitationem enim vitae quis. Sequi quia quidem aut non nihil sunt.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(24, 'Laury Rodriguez', 'https://via.placeholder.com/200x300.png/00bb66?text=+Faker+quo', 'Earum tempore illo iste doloremque nihil mollitia ratione. Et odio eum quia consequuntur et. Aliquid praesentium eaque beatae atque voluptatibus.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(25, 'Prof. Gabriel Hackett', 'https://via.placeholder.com/200x300.png/00ee33?text=+Faker+ipsum', 'Mollitia delectus doloremque asperiores sit. Ipsa ratione illo omnis iure ipsum et ut. Quisquam mollitia temporibus placeat est omnis corporis. Ullam repellat ut molestiae rerum eaque est.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(26, 'Clinton Kuhn', 'https://via.placeholder.com/200x300.png/001133?text=+Faker+voluptatem', 'Aut numquam quaerat officia et soluta. Minima ut officiis accusamus in adipisci laborum autem. Sint qui quia eum repellendus iste amet eos.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(27, 'Sierra Ratke', 'https://via.placeholder.com/200x300.png/002277?text=+Faker+quae', 'Excepturi omnis harum non vero ducimus adipisci pariatur veniam. Expedita ut fugiat in voluptatem maxime molestiae. Vel nemo dolor sunt debitis unde et saepe.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(28, 'Kailee Stanton', 'https://via.placeholder.com/200x300.png/00bb44?text=+Faker+voluptas', 'Blanditiis et aut quia atque. Ratione qui illum eos.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(29, 'Tyree Keeling', 'https://via.placeholder.com/200x300.png/0099ff?text=+Faker+qui', 'Et optio praesentium laudantium. Aut quod nulla id officia labore quia. Facilis velit totam quis velit.', '2021-08-30 08:50:28', '2021-08-30 08:50:28'),
(30, 'Anya Stiedemann DVM', 'https://via.placeholder.com/200x300.png/0055bb?text=+Faker+laborum', 'Ipsa natus fugiat eum. Debitis est nam dolor qui. Aperiam nulla distinctio neque qui fugiat quia.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(31, 'Prof. Max O\'Connell II', 'https://via.placeholder.com/200x300.png/005588?text=+Faker+molestias', 'Optio rerum id voluptatem eligendi et magnam. Sapiente molestiae natus temporibus qui sed iusto. Et non cumque cum saepe est voluptatibus.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(32, 'Dr. Aida Hane Jr.', 'https://via.placeholder.com/200x300.png/00cc22?text=+Faker+facilis', 'Velit explicabo ullam quas ut reprehenderit. Saepe quos voluptas rerum eum dolores. Modi esse perferendis vel optio exercitationem itaque natus. Quas provident adipisci fugit vel vel minus.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(33, 'Lamar Von', 'https://via.placeholder.com/200x300.png/00cc22?text=+Faker+ipsum', 'Officia nulla repellendus perspiciatis. Pariatur dolores reprehenderit voluptatem non ratione inventore et. Nemo voluptas cumque vero placeat itaque doloremque. Id in quae illo rerum.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(34, 'Virgie Bode', 'https://via.placeholder.com/200x300.png/000099?text=+Faker+expedita', 'Consequatur omnis modi sed rerum nihil sunt aut. Cum veritatis aut consequatur a aliquid. Rerum in sequi ipsum sit quae aliquid. Vel commodi aut perspiciatis nesciunt accusamus.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(35, 'Marcella Spinka', 'https://via.placeholder.com/200x300.png/00bb22?text=+Faker+sit', 'Nihil qui natus earum dolor a totam. Assumenda saepe quibusdam amet dolores.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(36, 'Prof. Ernesto Sawayn', 'https://via.placeholder.com/200x300.png/0011ff?text=+Faker+error', 'Rerum aut modi et expedita animi quibusdam sapiente consequatur. Minima vero velit et iusto eos ratione nemo praesentium. Maiores fugit eos deserunt quia eos omnis non. Ut qui sunt aut itaque.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(37, 'Amina Stehr II', 'https://via.placeholder.com/200x300.png/00cc11?text=+Faker+consequuntur', 'Consequatur voluptatibus sint voluptatem explicabo corrupti. Sit consectetur eos illo. Eum ad assumenda blanditiis. Eaque sint inventore dolorum nihil adipisci magnam.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(38, 'Lavonne Kuvalis DDS', 'https://via.placeholder.com/200x300.png/004400?text=+Faker+velit', 'Non maxime impedit qui ipsum quasi. Sed illum eum consequatur magni. Consequatur laborum sed voluptas aut. Autem sed aliquid est quam. Sit quam et et ipsum facilis.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(39, 'Athena Schuppe IV', 'https://via.placeholder.com/200x300.png/0011cc?text=+Faker+omnis', 'Aspernatur mollitia ea dolorum culpa. Consectetur alias quae omnis rerum sunt aliquid. Et quam adipisci aut dolorem enim quaerat consequatur quis.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(40, 'Angela Metz', 'https://via.placeholder.com/200x300.png/00cc88?text=+Faker+illum', 'Quis harum blanditiis rem qui doloribus ut. Quam repellat repellat et consequatur. Sit fuga est est eos. A amet odio qui ex architecto.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(41, 'Antonio Gutkowski', 'https://via.placeholder.com/200x300.png/005599?text=+Faker+dolore', 'Dolore sed amet voluptatem dolorem ut voluptas ut. Ullam voluptatem fugiat dicta voluptas aperiam et. Et numquam accusantium aut.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(42, 'Eliza Kling', 'https://via.placeholder.com/200x300.png/002233?text=+Faker+ut', 'Ut iure cum voluptatum non quia aspernatur non et. Officiis enim autem dolore modi ipsum cum quod. Quia amet fugit voluptatem aperiam molestiae fugit eaque qui.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(43, 'Prof. Sylvester Volkman PhD', 'https://via.placeholder.com/200x300.png/0011ff?text=+Faker+ducimus', 'Cum iusto qui laudantium voluptatem dolor dolorum at consequatur. Voluptatem distinctio ad esse pariatur. Dolores dolores qui ab qui odio sunt labore.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(44, 'Karolann McLaughlin', 'https://via.placeholder.com/200x300.png/008866?text=+Faker+voluptatem', 'Eum suscipit repudiandae mollitia vel ut. Inventore illum maiores quia. Voluptatem veritatis et quam et iusto ducimus sequi. Rerum reprehenderit veniam inventore.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(45, 'Anibal Kiehn', 'https://via.placeholder.com/200x300.png/005500?text=+Faker+quis', 'Nulla a facilis deserunt. Provident eum nobis sed et cumque eum nesciunt. Est est mollitia ea praesentium voluptatum accusantium ducimus.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(46, 'Dr. Emil Abernathy V', 'https://via.placeholder.com/200x300.png/0066bb?text=+Faker+assumenda', 'Libero ducimus cumque libero ratione nesciunt. Debitis enim impedit eum aut magnam. Est molestiae ipsum at.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(47, 'Prof. Manley Johns DVM', 'https://via.placeholder.com/200x300.png/0066ff?text=+Faker+nihil', 'Voluptatem eum nisi sequi iusto. Natus eveniet impedit neque officia enim repellendus mollitia. Provident ut doloremque facere alias. Et doloribus necessitatibus eaque ullam.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(48, 'Edgardo Kilback', 'https://via.placeholder.com/200x300.png/0055dd?text=+Faker+vel', 'Quaerat deleniti saepe aut sed reiciendis laborum. Unde fuga expedita eum tempora at. Laudantium quis dicta nesciunt architecto provident eos.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(49, 'Mara Kovacek', 'https://via.placeholder.com/200x300.png/00dd77?text=+Faker+soluta', 'Eos aut quasi id ullam. Modi tenetur repellendus dolores omnis cupiditate. Incidunt recusandae eligendi rem eos dicta dolore. Alias voluptate qui exercitationem ipsa.', '2021-08-30 08:50:29', '2021-08-30 08:50:29'),
(50, 'Mr. Gunnar Keeling DDS', 'https://via.placeholder.com/200x300.png/0077cc?text=+Faker+nostrum', 'Ut rerum autem illum tempore. Consequatur impedit quia nemo dolorem non. Voluptatum optio nam numquam sed similique. Quo ab porro esse.', '2021-08-30 08:50:29', '2021-08-30 08:50:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hex` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `colors`
--

INSERT INTO `colors` (`id`, `color`, `hex`, `created_at`, `updated_at`) VALUES
(1, 'Tia Gislason', '354856', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(2, 'Gunner Crona', '237491', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(3, 'Dr. Earline Hirthe II', '607318', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(4, 'Marques Osinski', '984681', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(5, 'Tanya Ferry', '389534', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(6, 'Mr. Jamel Jerde IV', '497630', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(7, 'Mrs. Fannie Mraz', '401108', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(8, 'Prof. Ramiro Witting', '257160', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(9, 'Zelda Pagac', '869240', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(10, 'Elyse Ernser', '168953', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(11, 'Mrs. Tianna Koch I', '137520', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(12, 'Dr. Lonny Fisher', '813010', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(13, 'Mrs. Leann Kassulke V', '434667', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(14, 'Mrs. Kira Streich', '896151', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(15, 'Mr. Ramiro Windler DVM', '986353', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(16, 'Blanche McClure', '699919', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(17, 'Prof. Junius Leuschke I', '883025', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(18, 'Pamela Spinka', '346532', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(19, 'Paige Stokes', '391696', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(20, 'Mrs. Meaghan Purdy', '557608', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(21, 'Anna Auer', '226348', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(22, 'Lela Koepp', '393453', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(23, 'Robbie Turner', '578179', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(24, 'Prof. Gaston Kuhn', '856675', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(25, 'Hermina Dickens', '545726', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(26, 'Mrs. Abbey Franecki', '956178', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(27, 'Kathlyn Willms PhD', '706173', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(28, 'Anika Fritsch', '750399', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(29, 'Aaliyah Hagenes V', '396657', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(30, 'Jackeline Weissnat', '626899', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(31, 'Darlene Reichert MD', '453954', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(32, 'Dr. Davion Walsh', '435848', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(33, 'Prof. Jaime Thiel', '760822', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(34, 'Meggie Kuhn', '374730', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(35, 'Caesar Gislason', '526794', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(36, 'Prof. Ashley Emard', '872650', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(37, 'Tillman Wilkinson', '659776', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(38, 'Pinkie Strosin', '557278', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(39, 'Tressa Rogahn', '743045', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(40, 'Ethelyn Carter', '122229', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(41, 'Prof. Nicholas Daniel DVM', '377624', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(42, 'Jerrod Cummerata PhD', '680210', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(43, 'Arden Hansen', '239601', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(44, 'Jeff Waelchi Sr.', '541398', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(45, 'Rosetta Davis', '968226', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(46, 'Russ Okuneva', '878541', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(47, 'Rafaela Swift', '832584', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(48, 'Christophe Cormier', '153902', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(49, 'Janick Ebert', '746441', '2021-08-30 08:50:30', '2021-08-30 08:50:30'),
(50, 'Dawson Yost', '147209', '2021-08-30 08:50:30', '2021-08-30 08:50:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(46, '2014_10_12_000000_create_users_table', 1),
(47, '2014_10_12_100000_create_password_resets_table', 1),
(48, '2019_08_19_000000_create_failed_jobs_table', 1),
(49, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(50, '2021_08_30_135803_create_products', 1),
(51, '2021_08_30_140855_create_categories', 1),
(52, '2021_08_30_141147_create_product_category', 1),
(53, '2021_08_30_141320_create_colors', 1),
(54, '2021_08_30_141434_create_product_color', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Wallace Cummings', '2114081', 'Quas distinctio autem laudantium aliquam facere error. Veritatis hic eaque nemo vero sed eum quae sint. Omnis nulla eos eaque impedit non omnis ea.', 'https://via.placeholder.com/200x300.png/0044bb?text=+Faker+consectetur', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(2, 'Giovanni Wiegand DDS', '2204390', 'Recusandae explicabo suscipit at vel earum ut. Similique in pariatur molestias labore. Magnam aut accusantium molestiae voluptatem ut fugiat. Recusandae iusto atque quos accusantium.', 'https://via.placeholder.com/200x300.png/00ee00?text=+Faker+vitae', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(3, 'Aisha Ryan', '1340960', 'Sunt incidunt iure quisquam vitae sint aspernatur laborum et. Quod quos est rerum et ex est corrupti.', 'https://via.placeholder.com/200x300.png/0066ee?text=+Faker+ex', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(4, 'Wilhelm Orn I', '2984263', 'Maiores et ut tempore ut ea cupiditate. Aliquam sint dolores omnis voluptas. Magnam et repellendus aut. Fugiat qui itaque quam et tempore beatae.', 'https://via.placeholder.com/200x300.png/00ff33?text=+Faker+minima', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(5, 'Mr. Samson Lindgren IV', '1950898', 'Omnis sint rerum maxime non dolorem. Praesentium et dolor maxime dignissimos sapiente officia et.', 'https://via.placeholder.com/200x300.png/002200?text=+Faker+illum', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(6, 'Kaley Gibson', '1645356', 'Corporis odit ipsa facere exercitationem ipsa recusandae laborum. Tenetur libero iste impedit. Quibusdam velit error unde aliquid.', 'https://via.placeholder.com/200x300.png/009966?text=+Faker+sed', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(7, 'Ms. Mireya Goyette Jr.', '1772332', 'Pariatur debitis officia quidem error non consequatur cumque. In incidunt consequuntur quibusdam dolorum sit facilis dolorem qui. Quas molestiae voluptas quo voluptatum qui exercitationem.', 'https://via.placeholder.com/200x300.png/003322?text=+Faker+sed', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(8, 'Dejon Torphy', '934978', 'Quae alias et est porro aut. Dolore eveniet et accusantium dolor. Odit est distinctio voluptatem qui vel minus officia. Ut quia est totam sit cupiditate.', 'https://via.placeholder.com/200x300.png/000044?text=+Faker+quo', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(9, 'Mr. Americo Nikolaus', '2451254', 'Dicta consequatur laborum cupiditate. Et qui et incidunt et iusto nobis laboriosam quas. Doloremque et incidunt iure aliquam accusantium quaerat.', 'https://via.placeholder.com/200x300.png/00bb77?text=+Faker+vel', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(10, 'Mitchel Waelchi', '1852333', 'Sapiente minus non eaque sed. Mollitia ipsa facere velit deserunt esse. Omnis omnis ea quis quisquam qui. Illo et facilis et nulla vel et.', 'https://via.placeholder.com/200x300.png/00bb33?text=+Faker+nostrum', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(11, 'Adeline Denesik', '1513138', 'Impedit sed eligendi eum in id. Ipsum fuga voluptate mollitia cumque et. Qui dolor aut fugiat cumque rerum sed dolorem et. Non quod necessitatibus et et animi minus rerum ratione.', 'https://via.placeholder.com/200x300.png/008822?text=+Faker+rerum', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(12, 'Michael Kerluke', '683395', 'Quo enim aliquid eum. Nostrum in dolore voluptatum aut impedit. Blanditiis iste pariatur magnam quia inventore ducimus eos quam. Omnis sint asperiores debitis qui sunt.', 'https://via.placeholder.com/200x300.png/004433?text=+Faker+maxime', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(13, 'Jocelyn McDermott', '507588', 'Quos voluptatem ratione fugiat cupiditate ullam. Ea facere atque neque et.', 'https://via.placeholder.com/200x300.png/00ff99?text=+Faker+nemo', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(14, 'Aimee Hamill', '1871609', 'Deserunt in consectetur rem non. Qui quas molestias a quas provident nisi cum. Dolorum omnis nihil voluptates quod dolorem temporibus omnis. Eius aut et accusamus aut ab quis voluptatum ut.', 'https://via.placeholder.com/200x300.png/00aa88?text=+Faker+itaque', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(15, 'Prof. Glennie Weimann', '2791770', 'Hic facere sed dolorem accusamus quaerat suscipit. Unde iusto qui aut recusandae voluptatum. Et sed minima libero quo perspiciatis pariatur dolor. Ut veniam cum eligendi ullam.', 'https://via.placeholder.com/200x300.png/002277?text=+Faker+dolore', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(16, 'Alphonso Trantow', '2964427', 'Magnam consequatur voluptates ex ea illo. Veniam ut perferendis distinctio doloribus qui molestiae consequatur. Rerum vero dignissimos velit eum.', 'https://via.placeholder.com/200x300.png/00dd33?text=+Faker+et', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(17, 'Missouri Gottlieb', '1335185', 'Corrupti eveniet nobis quasi. Et quod nesciunt neque expedita atque. Tempore qui ipsam similique minus impedit voluptatem. Repellendus nobis est sequi et assumenda.', 'https://via.placeholder.com/200x300.png/00eeaa?text=+Faker+eveniet', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(18, 'Josefa McCullough', '976747', 'Quia cupiditate natus illo aut eos. Qui laudantium velit voluptate minima. Et nihil inventore qui architecto dolores molestiae.', 'https://via.placeholder.com/200x300.png/00ee77?text=+Faker+harum', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(19, 'Scarlett Thompson', '596689', 'Necessitatibus impedit in atque cumque alias impedit. Totam omnis ullam architecto sed. Ea voluptate quis earum qui voluptas aut vero. Suscipit optio nemo reprehenderit sint.', 'https://via.placeholder.com/200x300.png/008855?text=+Faker+natus', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(20, 'Rosalee Labadie', '867075', 'Ad rerum unde unde ut. Vel qui doloremque impedit dicta. Sit omnis non dolor consequatur iure.', 'https://via.placeholder.com/200x300.png/0066ff?text=+Faker+quas', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(21, 'Corene Orn', '2213195', 'Esse magni sit accusantium sed fugiat explicabo ut. Laborum autem eos minus recusandae a. Minima unde ab aut. Cupiditate tenetur et libero consequatur facere aut in.', 'https://via.placeholder.com/200x300.png/009911?text=+Faker+eaque', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(22, 'Dr. Reba Shanahan DVM', '875527', 'Quo voluptatem tenetur quisquam sit qui voluptas. Necessitatibus est architecto eveniet autem. Sit est aperiam corrupti ut est. Alias cupiditate enim voluptas ut quibusdam natus in.', 'https://via.placeholder.com/200x300.png/004411?text=+Faker+quas', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(23, 'Ms. Lolita Leuschke', '1227965', 'Porro sit culpa amet tempora quia ipsa. Animi molestiae et earum quaerat sequi debitis odit cum. Voluptatibus vel numquam itaque temporibus officia esse et.', 'https://via.placeholder.com/200x300.png/003322?text=+Faker+dolores', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(24, 'Isadore Casper', '937893', 'Minus animi non odio impedit ipsa assumenda. Explicabo qui et sapiente culpa. Quas qui qui occaecati quo aliquam.', 'https://via.placeholder.com/200x300.png/009933?text=+Faker+consequuntur', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(25, 'Haylie Sporer DDS', '797483', 'Nemo qui enim ut quis consequatur. Sed fugiat eius natus quia. Aut et voluptatem tempora ab. Velit nobis nulla fugiat libero molestiae quasi adipisci.', 'https://via.placeholder.com/200x300.png/00eeaa?text=+Faker+illum', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(26, 'Rico Senger', '1560782', 'Beatae distinctio accusantium maiores et nihil expedita in. Voluptatem suscipit quos quia sit. Id nisi maiores adipisci quia iure voluptas. Quo doloremque doloremque ratione vero sunt.', 'https://via.placeholder.com/200x300.png/0066dd?text=+Faker+et', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(27, 'Gaston Kunde', '423928', 'Sit quis ad quia qui expedita voluptatem. A ut corrupti adipisci ab autem omnis autem. Dolore alias ullam est sunt dolore id. Sit aspernatur est aut explicabo tempora hic explicabo.', 'https://via.placeholder.com/200x300.png/006677?text=+Faker+adipisci', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(28, 'Jed Dooley', '453935', 'Quidem veritatis magnam et dolor. Illo nihil maxime temporibus commodi et a. Minima repellat magni suscipit omnis voluptatibus nobis. Temporibus possimus omnis cum autem qui non voluptas.', 'https://via.placeholder.com/200x300.png/00aaee?text=+Faker+est', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(29, 'Fred Weissnat DVM', '2420535', 'Enim consequatur consequatur eos non voluptate aut. Dolor sit omnis veritatis sint officiis. Adipisci est aut id.', 'https://via.placeholder.com/200x300.png/0055ff?text=+Faker+harum', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(30, 'Mariam Moore III', '1439501', 'Atque quia nobis voluptas a. Ducimus qui omnis qui sed incidunt odio. Sit laudantium maiores ipsam accusantium labore. Inventore odit nesciunt minus.', 'https://via.placeholder.com/200x300.png/0066bb?text=+Faker+et', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(31, 'Lance Becker IV', '150782', 'Quisquam sint vero repellat deleniti sed. Pariatur consequuntur illum earum temporibus. Quaerat perferendis nihil dignissimos. Omnis nam nulla voluptatem quasi.', 'https://via.placeholder.com/200x300.png/00bb99?text=+Faker+itaque', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(32, 'Dr. Donald Berge Sr.', '2486171', 'Ut quis ratione autem minima. Laudantium nisi commodi et doloribus inventore. Dolorum necessitatibus veritatis voluptas eaque et.', 'https://via.placeholder.com/200x300.png/0088ff?text=+Faker+at', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(33, 'Annamae Feest V', '412340', 'Laudantium voluptatibus autem qui quasi a quia ut. Sequi sed quia et qui sit doloremque. Natus dignissimos eum voluptas quis sint. Ut laudantium illum numquam et expedita omnis aut.', 'https://via.placeholder.com/200x300.png/00ee77?text=+Faker+fugiat', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(34, 'Saige Lehner', '173045', 'Maxime expedita autem ut ut. Repudiandae qui aut autem molestiae ipsa in perspiciatis. Qui dolor a sunt distinctio sint ut esse qui. Tempora beatae nulla blanditiis similique sed culpa.', 'https://via.placeholder.com/200x300.png/007733?text=+Faker+temporibus', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(35, 'Grayson Ruecker', '659442', 'Sequi porro et at nulla. Facilis corrupti magnam ipsa maiores quia consequuntur. Eos dolore voluptates fugiat est. Aut sed qui et voluptates.', 'https://via.placeholder.com/200x300.png/004488?text=+Faker+quas', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(36, 'Christa Gorczany', '546895', 'Totam praesentium qui voluptatem error. Consequatur voluptatem autem error possimus. Explicabo asperiores hic ipsa. Incidunt qui ut magni aut quod est.', 'https://via.placeholder.com/200x300.png/00ee33?text=+Faker+nobis', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(37, 'Vickie Kris DVM', '2946257', 'Sed perferendis eum sapiente perferendis. Rerum velit est vel exercitationem accusantium. Quia nam perferendis labore dignissimos odio sunt ut est. Qui magni explicabo totam veritatis.', 'https://via.placeholder.com/200x300.png/000088?text=+Faker+ab', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(38, 'Anastacio Prosacco', '211971', 'Labore rem est iure porro labore debitis. Voluptatem at corrupti voluptas. Quis numquam qui iusto occaecati in nostrum nostrum. Perferendis eum rem nostrum animi rerum nesciunt sit.', 'https://via.placeholder.com/200x300.png/0099ff?text=+Faker+et', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(39, 'Riley Raynor DVM', '2713017', 'Omnis nisi minus provident aut. Velit consequatur dolorem et libero. Minima quas repellat assumenda enim est id dolor.', 'https://via.placeholder.com/200x300.png/008866?text=+Faker+labore', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(40, 'Prof. Kale Anderson III', '1206154', 'Possimus voluptatibus sed eligendi numquam. Laboriosam architecto aut minima pariatur recusandae aut. Sunt quia voluptatibus aut rerum est aliquid quo voluptate.', 'https://via.placeholder.com/200x300.png/009944?text=+Faker+iure', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(41, 'Prof. Boris Fay', '2437065', 'Provident aliquid nihil nobis quos. Exercitationem cum ea aut sed qui aut odit numquam. Sequi corrupti quasi velit sit.', 'https://via.placeholder.com/200x300.png/00bb33?text=+Faker+quasi', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(42, 'Roel Hahn', '293637', 'Commodi nulla tempore qui magnam. Eveniet rerum dolorem repellat eius et pariatur. Aut unde molestiae veniam ut autem molestiae expedita. Sunt architecto ut ut quo voluptatem aspernatur.', 'https://via.placeholder.com/200x300.png/003322?text=+Faker+cupiditate', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(43, 'Jasmin Schiller', '1838342', 'Hic nostrum velit sed beatae. Accusamus saepe autem animi error sapiente voluptatem quia. Repellat vel iste laudantium est nemo quasi iure.', 'https://via.placeholder.com/200x300.png/0011bb?text=+Faker+veritatis', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(44, 'Destiney Kerluke', '1657275', 'Qui reiciendis tenetur earum. Ea debitis inventore consequatur aliquid rerum. Sapiente laudantium esse adipisci assumenda facere sequi.', 'https://via.placeholder.com/200x300.png/005500?text=+Faker+qui', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(45, 'Ivy Adams', '535531', 'Voluptas ratione sunt rerum architecto impedit. Rerum minima in necessitatibus eos officia et. Doloribus incidunt velit illum doloremque. Hic enim delectus veritatis ea nam. Eveniet deserunt rem et.', 'https://via.placeholder.com/200x300.png/009900?text=+Faker+recusandae', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(46, 'Amelia Koelpin', '2565716', 'Ea excepturi exercitationem quas minima asperiores. Debitis commodi beatae libero sapiente aspernatur. Unde est assumenda eum aut neque.', 'https://via.placeholder.com/200x300.png/00aa99?text=+Faker+quae', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(47, 'Rylee Yundt', '1333458', 'Sint ex sunt illum cumque. Dolores quam culpa itaque est omnis est tenetur. Architecto quia hic qui aliquid quae cum.', 'https://via.placeholder.com/200x300.png/00ddbb?text=+Faker+voluptatem', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(48, 'Israel Corwin', '1622464', 'Dignissimos consequatur ad quasi deserunt asperiores culpa sequi et. Amet dolores et voluptates quod perferendis dolorem. Quo voluptatem dolor neque exercitationem. Sit et quo in laborum.', 'https://via.placeholder.com/200x300.png/003300?text=+Faker+culpa', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(49, 'Dr. Mose Hickle DVM', '1309923', 'Voluptates et ut molestias quo in est aspernatur. Corrupti deleniti et ut sapiente. Qui sint mollitia adipisci officia.', 'https://via.placeholder.com/200x300.png/006644?text=+Faker+in', '2021-08-30 08:50:27', '2021-08-30 08:50:27'),
(50, 'Darrell Runolfsdottir', '2736939', 'Quam odio quasi ducimus minus occaecati vel non. Maxime voluptatem sed quo sit unde. Vero et ea itaque quidem voluptas sequi.', 'https://via.placeholder.com/200x300.png/00aa22?text=+Faker+facere', '2021-08-30 08:50:27', '2021-08-30 08:50:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_category`
--

CREATE TABLE `product_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_category`
--

INSERT INTO `product_category` (`id`, `product_id`, `category_id`) VALUES
(1, 7, 36),
(2, 37, 46),
(3, 47, 15),
(4, 42, 15),
(5, 32, 14),
(6, 33, 40),
(7, 28, 37),
(8, 47, 31),
(9, 13, 29),
(10, 30, 24),
(11, 8, 23),
(12, 27, 48),
(13, 4, 48),
(14, 42, 26),
(15, 22, 17),
(16, 39, 24),
(17, 37, 4),
(18, 38, 34),
(19, 44, 28),
(20, 38, 31),
(21, 42, 47),
(22, 38, 24),
(23, 26, 42),
(24, 45, 24),
(25, 18, 20),
(26, 47, 21),
(27, 47, 26),
(28, 35, 25),
(29, 21, 32),
(30, 28, 12);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_color`
--

CREATE TABLE `product_color` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_color`
--

INSERT INTO `product_color` (`id`, `product_id`, `color_id`) VALUES
(1, 21, 37),
(2, 22, 24),
(3, 25, 38),
(4, 12, 20),
(5, 5, 26),
(6, 37, 49),
(7, 14, 26),
(8, 25, 18),
(9, 27, 16),
(10, 35, 31),
(11, 14, 27),
(12, 16, 47),
(13, 3, 37),
(14, 46, 48),
(15, 43, 22),
(16, 23, 38),
(17, 22, 45),
(18, 6, 42),
(19, 8, 29),
(20, 23, 27),
(21, 44, 6),
(22, 20, 49),
(23, 26, 48),
(24, 39, 34),
(25, 43, 26),
(26, 23, 17),
(27, 48, 23),
(28, 15, 7),
(29, 10, 46),
(30, 40, 40);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Golda Hayes', 'viviane.windler@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SnQ0eTkynB', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(2, 'Clifton Boehm Jr.', 'lucio.terry@example.net', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ydu6XuGllZ', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(3, 'Dedric Hudson', 'jaida92@example.org', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'muQI7kwbTX', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(4, 'Carroll Kemmer DDS', 'serenity.hill@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NOHc4xCZQ0', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(5, 'Stephon Brakus', 'gregorio.schuppe@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lFGN07E0vY', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(6, 'Teresa Reynolds', 'chyna.christiansen@example.net', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '9G3VJ7aLxE', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(7, 'Arden Metz', 'abel38@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AAiKIL7WN5', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(8, 'Carolanne Koch IV', 'flatley.reina@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AdxJzXiccr', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(9, 'Agnes Cummerata', 'kulas.ciara@example.net', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7Phyn2jqRT', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(10, 'Buster Abernathy', 'keeling.zakary@example.net', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'F1lbIZta7i', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(11, 'Ova Wuckert', 'brigitte88@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Dt6oyfeMIo', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(12, 'Guido Powlowski', 'gnader@example.org', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DPVJf6mNIL', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(13, 'Reymundo Donnelly', 'nohara@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6PA1cqrsLc', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(14, 'Prof. Rickey Wehner', 'fschiller@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7v4tqQuk3J', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(15, 'Mrs. Elinor Pfannerstill DDS', 'roy.lynch@example.net', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XzcsgJlOUc', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(16, 'Jacinto Nienow', 'breanna86@example.org', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '252gjyxcIZ', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(17, 'Mrs. Jacquelyn Pouros III', 'katlynn.rolfson@example.net', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'hxIV211M3L', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(18, 'Dr. Jolie Parker', 'daniela04@example.org', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CnKRDasEbY', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(19, 'Dr. Francis Moen I', 'roslyn.bechtelar@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kLNO9C3Eq2', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(20, 'Elva Beier', 'brandy.ritchie@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Sj1NhHzGeN', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(21, 'Yoshiko Keebler', 'akeem40@example.net', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ctKjtvXXv2', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(22, 'Prof. Davon Gerhold V', 'weimann.rollin@example.org', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3mAcAA6R1e', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(23, 'Tracy Fadel', 'davion.labadie@example.org', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YkAqsAYJv0', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(24, 'Shannon Dibbert', 'harris.cedrick@example.org', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'm0rYZG6QAv', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(25, 'Mr. Melvin Stanton MD', 'bpollich@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1SKx9OUZhZ', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(26, 'Brooks Bernhard', 'lroberts@example.net', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kUEhOHKNO7', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(27, 'Mallory Metz', 'kassulke.garfield@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cKEcx2R0es', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(28, 'Prof. Missouri Labadie IV', 'abdul.haag@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'WzQgEVvW78', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(29, 'Mr. Moriah Zieme', 'wkessler@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'e5C9PnSAHb', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(30, 'Rylee Toy', 'orn.delmer@example.com', '2021-08-30 08:50:32', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '89QeGXu1dL', '2021-08-30 08:50:32', '2021-08-30 08:50:32'),
(31, 'HoangMinh', 'hoangminhcp10@gmail.com', NULL, '$2y$10$1VoriY.pIWtLrhDACIPQUeOTdojpYyvQvfi8wtwbfloJCVC4/qHO2', 'administrator', '2021-08-30 08:55:14', '2021-08-30 08:55:14');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT cho bảng `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT cho bảng `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
