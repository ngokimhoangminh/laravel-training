* Giống nhau:
	- Var và let đều là từ khoá.
	- Var và let cùng được dùng để khai báo biến trong JavaScript
* Khác nhau:
	- Phạm vi của biến sử dụng var là phạm vi hàm số hoặc bên ngoài hàm số, biến toàn cục.
	- Phạm vi của biến số sử dụng let là phạm vi một khối, xác định bởi cặp {}.