.demo: Chọn tất cả các phần tử mà thuộc tính class của nó giá trị là demo 
#open: Chọn phần tử có thuộc tính id của nó có giá trị là open.
*: Chọn tất cả phần tử 
div: Chọn tất cả các phần tử thẻ <div>.
div, p: Chọn tất cả các phần tử thẻ <div> và thẻ <p>
div p: Chọn tất cả các phần tử thẻ <p> và nằm bên trong thẻ <div>
div > p:  Chọn tất cả các phần tử thẻ <p> là con trực tiếp của thẻ <div>
div + p: Chọn tất cả các phần tử thẻ <p> được đặt phía sau <div>
div ~ p: Chọn tất cả các phần tử thẻ <p> được đặt trước bởi một phần tử thẻ <div>
[target]: Chọn tất cả các phần tử có cùng thuộc tính.
[target=_blank]: Chọn tất cả các phần tử có thuộc tính ( target=”_blank”)
[lang|=en]: Chọn tất cả các phần tử có giá trị thuộc tính “lang” bắt đầu bằng “en”
a[href^=”https”]: Chọn tất cả các phần tử thẻ <a> có giá trị thuộc tính “href” bắt đầu bằng “https”.
a[href$=”.com”]: Chọn tất cả các phần tử thẻ <a> có giá trị thuộc tính “href” kết thúc bằng ”.com”
a[href*=”welcome”]: Chọn tất cả các phần tử thẻ <a> có giá trị thuộc tính “href” chứa chuỗi ”welcome”.
:active (a:active): Chọn tất cả các liên kết được kích vào.
::after (p::after): Chèn thêm nội dung ngay phía sau của các phần tử thẻ <p>
::before (p::before): Chèn thêm nội dung ngay phía trước của các phần tử thẻ <p>
:checked (input:checked): Chọn tất cả các phần tử thẻ <input> đang được chọn.
:disabled (input:disabled): Chọn tất cả các phần tử thẻ <input> đang được vô hiệu hoá (disabled)
:enabled (input:enabled): Chọn tất cả các phần tử thẻ <input> đang được kích hoạt.
:empty (p:empty): Chọn tất cả các thẻ <p> không chứa thẻ con.
::first-letter (p::first-letter): Chọn kí tự đầu tiên của phần tử thẻ  <p>
::first-line (p::first-line): Chọn dòng đầu tiên của các phần tử thẻ <p>
:first-of-type (p:first-of-type): Chọn tất cả các phần tử thẻ <p> có phần tử đầu tiên <p> là phần tử cha
:focus (input:focus): Chọn các phần tử <input> nhận focus
:hover (a:hover): Thực hiện các thay đổi khi khi chuột di chuyển qua thẻ a.
:visited (a:visited): Thực hiện thay đổi với tất cả các liên kết thẻ a được truy cập.
:in-range (input:in-range): Chọn phần tử <input> có giá trị  trong phạm vi nhất định.
:out-of-range (input:out-of-range): Chọn tất cả các phần tử <input> đầu vào có giá trị ngoài một phạm vi nhất định.
:invalid (input:invalid): Chọn tất cả các phần tử <input> có giá trị không hợp lệ
:first-child (p:first-child): Chọn các phần tử thẻ <p> là phần tử đầu tiên của phần tử cha.
:last-child (p:last-child): Chọn tất cả các phần tử  <p> là phần tử con cuối cùng của phần tử cha.
:nth-child(n) (p:nth-child(2)): Chọn tất cả các phần tử thẻ <p> là phần tử thứ hai của phần tử cha.
p:nth-last-of-type(2): Chọn tất cả các phần tử thẻ <p> là phần tử thứ hai của phần tử cha, tính từ phần tử con cuối cùng.
:only-of-type (p:only-of-type): Chọn tất cả các phần tử thẻ <p> là thuộc tính duy nhất của phần tử cha.
:only-child (p:only-child): Chọn tất cả các phần tử thẻ <p> là con duy nhất của phần tử cha.
:last-of-type (p:last-of-type): Chọn tất cả các phần tử thẻ <p> là thuộc tính cuối cùng của phần tử cha.
:link (a:link): Chọn tất cả các liên kết khi chưa được click.
:not(selector) (:not(p)): Chọn tất cả các phần tử không phải là một phần tử <p>.
:optional (input:optional): Chọn tất cả các phần tử <input> đầu vào không có thuộc tính “required”
:read-only (input:read-only): Chọn tất cả các phần tử <input> đầu vào có thuộc tính xác định “readonly”.
:required (input:required): Chọn tất cả các phần tử <input> đầu vào có thuộc tính  “required” xác định.
:root: Chọn các phần tử gốc của văn bản.
::selection: Chọn các phần tử được người dùng lựa chọn.
:valid (input:valid): Chọn tất cả các phần tử đầu vào có một giá trị hợp lệ.