var gulp = require("gulp");
var sass = require("gulp-sass")(require('sass'));
var rename = require("gulp-rename");

function css() {
    return gulp.src('scss/style.scss')
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .pipe(rename(function (path) {
            path.basename = "style";
            path.extname = ".min.css";
        }))
        .pipe(gulp.dest('css/'));
}

function watch() {
    css();
    gulp.watch('scss/*.scss', css);//trình duyệt tự động reload khi bất kỳ file scss được lưu
}

gulp.task('default', watch);
