$(document).ready(function($) {
  $("form").submit(function() { return false; });
  deleteImage();
});
var count=0;
function validateEmail(email) {
	const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{3,}))$/;
	//const re=[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2, 4}$;
	return re.test(email);
}
function Enablesubmit()
{
	let username=$('#username').val();
	let password=$('#password').val();
	let password_confirm=$('#password_confirm').val();
	let email=$('#email').val();
	let phone=$('#phone').val();
	let gender=$('#gender').val();
	let image=$('#image').val();
	let checkError=false;

	if(username.length == 0)
	{
		$('#validationEmpty01').css('display','block');
		checkError=true;
	}else
	{
		$('#validationEmpty01').css('display','none');
		if(username.length < 3)
		{
			checkError=true;
			$('#validationServer01').css('display','block');
			$('#username').addClass('is-invalid');
		}else{
			checkError=false;
			$('#validationServer01').css('display','none');
			$('#username').removeClass('is-invalid');
			$('#username').addClass('is-valid');
		}	
	}

	if(password.length == 0)
	{
		$('#validationEmpty02').css('display','block');
		checkError=true;
	}else{
		checkError=false;
		$('#validationEmpty02').css('display','none');
		$('#password').addClass('is-valid');
	}
	//Passwprd confirm
	if(password_confirm.length == 0)
	{
		checkError=true;
		$('#validationEmpty03').css('display','block');
	}else{
		$('#validationEmpty03').css('display','none');
		if(password_confirm != password)
		{
			checkError=true;
			$('#validationServer03').css('display','block');
			$('#password_confirm').addClass('is-invalid');
		}else{
			checkError=false;
			$('#validationServer03').css('display','none');
			$('#password_confirm').removeClass('is-invalid');
			$('#password_confirm').addClass('is-valid');
		}
	}
	//Email
	if(email.length == 0)
	{
		checkError=true;
		$('#validationEmpty04').css('display','block');
	}else{
		$('#validationEmpty04').css('display','none');
		if(!validateEmail(email))
		{
			checkError=true;
			$('#validationServer04').css('display','block');
			$('#email').addClass('is-invalid');
		}else{
			checkError=false;
			$('#validationServer04').css('display','none');
			$('#email').removeClass('is-invalid');
			$('#email').addClass('is-valid');
		}
	}

	//
	if(phone.length == 0)
	{
		checkError=true;
		$('#validationEmpty05').css('display','block');
	}else{
		$('#validationEmpty05').css('display','none');
		if(phone.length!=10)
		{
			checkError=true;
			$('#validationServer05').css('display','block');
			$('#phone').addClass('is-invalid');
		}else{
			checkError=false;
			$('#validationServer05').css('display','none');
			$('#phone').removeClass('is-invalid');
			$('#phone').addClass('is-valid');
		}
	}
	if(gender.length == 0)
	{
		checkError=true;
		$('#validationEmpty06').css('display','block');
	}else{
		checkError=false;
		$('#validationEmpty06').css('display','none');
		$('#gender').addClass('is-valid');
	}
	if(image.length == 0)
	{
		checkError=true;
		$('#validationEmpty07').css('display','block');
	}else{
		checkError=false;
		$('#validationEmpty07').css('display','none');
	}
	if(checkError)
	{
		swal({
		  title: "Thất bại !!!",
		  text: "Vui lòng kiểm tra lại dữ liệu",
		  icon: "error",
		  button: "Đóng",
		});
	}else{
		swal({
		  title: "Thành công!",
		  text: "Bạn đã đăng ký tài khoàn thành công!",
		  icon: "success",
		  button: "Đăng nhập!",
		});
	}
} 
$("#image").on('change', function () {
	$('#validationEmpty07').css('display','none');
    var totalFiles = $(this)[0].files.length;
    var imgUrl = $(this)[0].value;
    let image_nanme=$('#image').val();
    var photo_tail = imgUrl.substring(imgUrl.lastIndexOf('.') + 1).toLowerCase();
    var image = $("#image-form");
    count++;
    if (photo_tail == "gif" || photo_tail == "png" || photo_tail == "jpg" || photo_tail == "jpeg") {
        if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                reader.onload = function (e) {
                 	$('#image-form').append(function() {
        				return '<div class="image_item image_item'+count+' mr-2"><i class="fas fa-trash-alt btn-delete" onclick="deleteImage('+count+')"></i><img src="'+e.target.result +'" class="thumb-image'+count+'" alt=""></div>';
    				});
                 }
                ;
                reader.readAsDataURL($(this)[0].files[0]);
        } else {
            swal ( "Oops" ,  "Trình duyệt này không hỗ trợ FileReader!" ,  "error" );
        }
    } else {
         swal ( "Oops" ,  "Làm ơn chỉ chọn hình ảnh!" ,  "error" );
    }
 });
function deleteImage(id)
{
	$(".image_item"+id).remove();
	document.getElementById("image").value = "";
}
